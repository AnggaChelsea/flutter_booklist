import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:networkingapi/models/book_list_response.dart';

class BookListPage extends StatefulWidget {
  const BookListPage({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _BookListPageState createState() => _BookListPageState();
}

class _BookListPageState extends State<BookListPage> {
  GetBook? bookList;
  fetchBookApi() async {
    // This example uses the Google Books API to search for books about http.

    var url = Uri.parse('https://api.itbook.store/1.0/new');
    var response = await http.get(url);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    if (response.statusCode == 200) {
      final jsonBookList = jsonDecode(response.body);
      bookList = GetBook.fromJson(jsonBookList);
      setState(() {});
    }

    print(await http.read(Uri.https('example.com', 'foobar.txt')));
  }

  void initState() {
    super.initState();
    fetchBookApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Book Katalog"),
      ),
      body: Container(
        child: bookList == null
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                //untuk memberitahu bahwa list book tak terhingga
                itemCount: bookList!.books!.length,

                ////
                itemBuilder: (context, index) {
                  final currentBook = bookList!.books![index];
                  return Row(
                    children: [
                      Image.network(
                        currentBook.image!,
                        height: 100,
                      ),
                      Expanded(
                        //use expanded buat text sampe ujung layar gak bakal melebihi
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(currentBook.title!),
                              Text(currentBook.subtitle!),
                              Align(
                                  alignment: Alignment.bottomRight,
                                  child: Text(currentBook.price!)),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                },
              ),
      ),
    );
  }
}
